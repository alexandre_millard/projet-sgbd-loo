package com.application.lab_reservation;

import com.controller.lab_reservation.ReservationController;
import com.controller.lab_reservation.PaymentController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;

public class LabReservation extends Application {
    public static BorderPane root = new BorderPane();

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {

        // Reservation View and Controller Init
        FXMLLoader reservationLoader = new FXMLLoader(getClass().getResource("/view/lab_reservation/ReservationView.fxml"));
        reservationLoader.setController(new ReservationController());
        root.setCenter(reservationLoader.load());

        // Reservation validation View and Controller Init
        FXMLLoader paymentLoader = new FXMLLoader(getClass().getResource("/view/lab_reservation/PaymentView.fxml"));
        paymentLoader.setController(new PaymentController());

        ReservationController reservationController = reservationLoader.getController();
        reservationController.paymentLoader = paymentLoader;

        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Lab Reservation");
        primaryStage.show();
    }

    public static BorderPane getRoot()
    {
        return root;
    }

}