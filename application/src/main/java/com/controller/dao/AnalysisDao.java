package com.controller.dao;

import com.model.Analysis;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public final class AnalysisDao {
    public static List<Analysis> getAllAnalysis(EntityManager em) {
        String queryString = "SELECT a FROM Analysis a";

        TypedQuery<Analysis> query = em.createQuery(queryString, Analysis.class);

        return query.getResultList();
    }
}
