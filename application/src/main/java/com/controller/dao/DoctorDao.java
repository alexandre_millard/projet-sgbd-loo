package com.controller.dao;

import com.model.Doctor;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public final class DoctorDao {
    public static List<Doctor> getAllDoctors(EntityManager em) {
        String queryString = "SELECT d FROM Doctor d";

        TypedQuery<Doctor> query = em.createQuery(queryString, Doctor.class);

        return query.getResultList();
    }
}