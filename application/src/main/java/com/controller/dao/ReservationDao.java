package com.controller.dao;

import com.model.Analysis;
import com.model.Doctor;
import com.model.Reservation;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;

public final class ReservationDao {
    /**
     * Searches for the first available reservation for an analysis and returns a new (non-persisted) reservation object corresponding to the found reservation.
     * @param em EntityManager to get data from database.
     * @param analysis Analysis to check for a free reservation.
     * @return New reservation object with the date, analysis and doctor.
     */
    public static Reservation getBestReservationDate(EntityManager em, Analysis analysis) {
        LocalDate reservationDate = LocalDate.now();

        List<Doctor> doctorList = DoctorDao.getAllDoctors(em);

        if (doctorList.isEmpty()) {
            throw new NoSuchElementException("No doctors are available for the specified analysis. Please try again later.");
        }

        for (Doctor d : doctorList) {
            if (d.getAnalysisList().contains(analysis)) continue;
            throw new NoSuchElementException("No doctors are available for the specified analysis. Please try again later.");
        }

        List<Reservation> reservationList = getAllReservations(em);

        Reservation reservation = new Reservation();

        if (!reservationList.isEmpty()) {
            Doctor reservationDoctor = null;

            while (reservationDoctor == null) {
                reservationDate = reservationDate.plusDays(1);

                for (Doctor d : doctorList) {
                    if (!d.getAnalysisList().contains(analysis)) continue;
                    for (Reservation r : reservationList) {
                        if (r.getDate().equals(reservationDate) && r.getDoctor() == d) {
                            reservationDoctor = null;
                            break;
                        } else {
                            reservationDoctor = d;
                        }
                    }
                }
            }
            reservation.setDoctor(reservationDoctor);
        } else {
            for (Doctor d : doctorList) {
                if (d.getAnalysisList().contains(analysis)) {
                    reservation.setDoctor(d);
                    reservationDate = reservationDate.plusDays(1);
                }
            }
        }

        reservation.setDate(reservationDate);
        reservation.setAnalysis(analysis);

        return reservation;
    }

    public static List<Reservation> getAllReservations(EntityManager em) {
        String queryString = "SELECT r FROM Reservation r";

        TypedQuery<Reservation> query = em.createQuery(queryString, Reservation.class);

        return query.getResultList();
    }
}
