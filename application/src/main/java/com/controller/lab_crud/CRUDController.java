package com.controller.lab_crud;

import com.controller.dao.AnalysisDao;
import com.controller.dao.DoctorDao;
import com.model.Analysis;
import com.model.Doctor;
import com.model.SocialSecurityNumber;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

/**
 * Used to control the CRUD window's elements.
 * Uses CRUDView.fxml.
 */
public class CRUDController {
    private EntityManager em;

    @FXML
    private Button addAnalysis;

    @FXML
    private Button addDoctor;

    @FXML
    private ComboBox<Analysis> analysisComboBox;

    @FXML
    private TextField analysisDescText;

    @FXML
    private Label analysisIDLabel;

    @FXML
    private TextField analysisNameText;

    @FXML
    private Button deleteAnalysis;

    @FXML
    private Button deleteDoctor;

    @FXML
    private TextField doctorAnalysisText;

    @FXML
    private ComboBox<Doctor> doctorComboBox;

    @FXML
    private TextField doctorFirstNameText;

    @FXML
    private TextField doctorLastNameText;

    @FXML
    private TextField doctorSSNText;

    @FXML
    private TextField doctorSalaryText;

    @FXML
    private Button updateAnalysis;

    @FXML
    private Button updateDoctor;

    public CRUDController() {
    }

    @FXML
    public void initialize() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("lab_reservation");
        em = emf.createEntityManager();

        resetAnalysisComboBox();
        resetDoctorComboBox();

        analysisComboBox.setOnAction(this::onAnalysisSelection);
        doctorComboBox.setOnAction(this::onDoctorSelection);
    }

    /**
     * Empties and fills up combobox with all analysis.
     */
    void resetAnalysisComboBox() {
        analysisComboBox.getItems().clear();
        analysisComboBox.getItems().add(null);
        analysisComboBox.getItems().addAll(AnalysisDao.getAllAnalysis(em));
    }

    /**
     * Empties and fills up combobox with all doctors.
     */
    void resetDoctorComboBox() {
        doctorComboBox.getItems().clear();
        doctorComboBox.getItems().add(null);
        doctorComboBox.getItems().addAll(DoctorDao.getAllDoctors(em));
    }

    /**
     * Adds an analysis to the database.
     */
    @FXML
    void addAnalysisAction(ActionEvent unused) {
        if (!analysisNameText.getText().equals("") && !analysisDescText.getText().equals("")) {
            Analysis analysis = new Analysis();
            analysis.setName(analysisNameText.getText());
            analysis.setDescription(analysisDescText.getText());

            em.getTransaction().begin();
            em.persist(analysis);
            em.getTransaction().commit();

            analysisNameText.clear();
            analysisDescText.clear();

            resetAnalysisComboBox();
        }
    }

    /**
     * Adds a doctor to the database.
     */
    @FXML
    void addDoctorAction(ActionEvent unused) {
        if (!doctorFirstNameText.getText().equals("") && !doctorLastNameText.getText().equals("")
                && !doctorSSNText.getText().equals("") && !doctorSalaryText.getText().equals("") && !doctorAnalysisText.getText().equals("")) {
            Doctor doctor = new Doctor();
            doctor.setFirstName(doctorFirstNameText.getText());
            doctor.setLastName(doctorLastNameText.getText());
            doctor.setSsn(new SocialSecurityNumber(doctorSSNText.getText()));
            doctor.setSalary(Float.parseFloat(doctorSalaryText.getText()));

            String[] analysisIDs = doctorAnalysisText.getText().split(";");

            em.getTransaction().begin();

            for (String analysisIDstr : analysisIDs) {
                int analysisID = Integer.parseInt(analysisIDstr);
                Analysis analysis = em.find(Analysis.class, analysisID);

                if (analysis != null) {
                    doctor.addAnalysis(analysis);
                }
            }

            em.persist(doctor);
            em.getTransaction().commit();

            doctorFirstNameText.clear();
            doctorLastNameText.clear();
            doctorSSNText.clear();
            doctorSalaryText.clear();
            doctorAnalysisText.clear();

            resetDoctorComboBox();
        }
    }

    /**
     * Deletes an analysis from the database.
     */
    @FXML
    void deleteAnalysisAction(ActionEvent unused) {
        Analysis analysis = analysisComboBox.getValue();

        em.getTransaction().begin();
        em.remove(analysis);
        em.getTransaction().commit();

        resetAnalysisComboBox();
    }

    /**
     * Deletes a doctor from the database.
     */
    @FXML
    void deleteDoctorAction(ActionEvent unused) {
        Doctor doctor = doctorComboBox.getValue();

        em.getTransaction().begin();
        for(Iterator<Analysis> iterator = doctor.getAnalysisList().iterator(); iterator.hasNext();) {
            Analysis analysis = iterator.next();
            iterator.remove();
            analysis.getDoctors().remove(doctor);
        }

        em.remove(doctor);
        em.getTransaction().commit();

        resetDoctorComboBox();
    }

    /**
     * Updates an analysis in the database.
     */
    @FXML
    void updateAnalysisAction(ActionEvent unused) {
        if (!analysisNameText.getText().equals("") && !analysisDescText.getText().equals("")) {
            Analysis analysis = analysisComboBox.getValue();

            em.getTransaction().begin();
            analysis.setName(analysisNameText.getText());
            analysis.setDescription(analysisDescText.getText());
            em.getTransaction().commit();

            analysisNameText.clear();
            analysisDescText.clear();

            resetAnalysisComboBox();
        }
    }

    /**
     * Updates a doctor in the database.
     */
    @FXML
    void updateDoctorAction(ActionEvent unused) {
        if (!doctorFirstNameText.getText().equals("") && !doctorLastNameText.getText().equals("")
                && !doctorSSNText.getText().equals("") && !doctorSalaryText.getText().equals("") && !doctorAnalysisText.getText().equals("")) {
            Doctor doctor = doctorComboBox.getValue();

            em.getTransaction().begin();
            doctor.setFirstName(doctorFirstNameText.getText());
            doctor.setLastName(doctorLastNameText.getText());
            doctor.setSsn(new SocialSecurityNumber(doctorSSNText.getText()));
            doctor.setSalary(Float.parseFloat(doctorSalaryText.getText()));

            for(Iterator<Analysis> iterator = doctor.getAnalysisList().iterator(); iterator.hasNext();) {
                Analysis analysis = iterator.next();
                iterator.remove();
                analysis.getDoctors().remove(doctor);
            }

            String[] analysisIDs = doctorAnalysisText.getText().split(";");

            for (String analysisIDstr : analysisIDs) {
                int analysisID = Integer.parseInt(analysisIDstr);
                Analysis analysis = em.find(Analysis.class, analysisID);

                if (analysis != null) {
                    doctor.addAnalysis(analysis);
                }
            }

            em.getTransaction().commit();

            doctorFirstNameText.clear();
            doctorLastNameText.clear();
            doctorSSNText.clear();
            doctorSalaryText.clear();
            doctorAnalysisText.clear();

            resetDoctorComboBox();
        }
    }

    /**
     * Loads the selected analysis' information in the text fields.
     */
    @FXML
    public void onAnalysisSelection(ActionEvent unused) {
        Analysis analysis = analysisComboBox.getSelectionModel().getSelectedItem();

        if (analysis == null) {
            analysisNameText.clear();
            analysisDescText.clear();
            analysisIDLabel.setText("No ID");

            addAnalysis.setVisible(true);
            updateAnalysis.setVisible(false);
            deleteAnalysis.setVisible(false);

        } else {
            analysisNameText.setText(analysis.getName());
            analysisDescText.setText(analysis.getDescription());
            analysisIDLabel.setText("ID : " + analysis.getId());

            addAnalysis.setVisible(false);
            updateAnalysis.setVisible(true);
            deleteAnalysis.setVisible(true);
        }
    }

    /**
     * Loads the selected doctor's information in the text fields.
     */
    @FXML
    public void onDoctorSelection(ActionEvent unused) {
        Doctor doctor = doctorComboBox.getSelectionModel().getSelectedItem();

        if (doctor == null) {
            doctorFirstNameText.clear();
            doctorLastNameText.clear();
            doctorSSNText.clear();
            doctorSalaryText.clear();
            doctorAnalysisText.clear();

            addDoctor.setVisible(true);
            updateDoctor.setVisible(false);
            deleteDoctor.setVisible(false);
        } else {
            doctorFirstNameText.setText(doctor.getFirstName());
            doctorLastNameText.setText(doctor.getLastName());
            doctorSSNText.setText(doctor.getSsn().toString());
            doctorSalaryText.setText(Float.toString(doctor.getSalary()));

            String text = "";

            for (Analysis analysis : doctor.getAnalysisList()) {
                text += analysis.getId() + ";";
            }

            doctorAnalysisText.setText(text);

            addDoctor.setVisible(false);
            updateDoctor.setVisible(true);
            deleteDoctor.setVisible(true);
        }
    }
}
