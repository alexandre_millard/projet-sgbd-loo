package com.controller.lab_reservation;

import com.model.Person;
import com.model.Reservation;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Date;

public class PaymentController {
    private EntityManager em;

    @FXML
    private TextField creditCardNumberInput;

    @FXML
    private TextField cvvInput;

    @FXML
    private TextField expirationInput;

    @FXML
    private Button submitButton;

    @FXML
    private Label warningLabel;

    private Person patient;

    private Reservation reservation;

    public PaymentController() {
    }

    @FXML
    public void initialize() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("lab_reservation");
        em = emf.createEntityManager();
    }

    public void setPatient(Person patient) {
        this.patient = patient;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

    @FXML
    void onSubmit(ActionEvent event) {
        if (creditCardNumberInput.getText().length() != 16 || cvvInput.getText().length() != 3 || expirationInput.getText().isEmpty()) {
            warningLabel.setVisible(true);
            warningLabel.setText("There was a problem with the payment. Please verify the information you have entered.");
        } else {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/yy");
            simpleDateFormat.setLenient(false);
            Date expiry;
            boolean expired = true;
            try {
                expiry = simpleDateFormat.parse(expirationInput.getText());
                expired = expiry.before(new Date());
            } catch (ParseException e) {
                warningLabel.setVisible(true);
                warningLabel.setText("Your card's date is invalid. Please enter a valid card and try again!");
            }

            if (expired) {
                warningLabel.setVisible(true);
                warningLabel.setText("Your card is expired. Please enter a valid card and try again!");
            } else {
                // Save patient
                em.getTransaction().begin();
                em.persist(patient);
                em.getTransaction().commit();

                // Save reservation
                em.getTransaction().begin();
                reservation.setPerson(patient);
                em.persist(reservation);
                em.getTransaction().commit();

                Alert validationDialog = new Alert(Alert.AlertType.INFORMATION);
                validationDialog.setTitle("Reservation Confirmed");
                validationDialog.setHeaderText("Your reservation for " + reservation.getDate().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)) + " has been confirmed");
                validationDialog.setContentText("See you soon!");

                validationDialog.showAndWait();

                System.exit(1);
            }
        }
    }
}