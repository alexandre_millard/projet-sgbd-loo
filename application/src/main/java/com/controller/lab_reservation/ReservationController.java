package com.controller.lab_reservation;

import com.application.lab_reservation.LabReservation;
import com.controller.dao.AnalysisDao;
import com.controller.dao.ReservationDao;
import com.model.Analysis;
import com.model.Person;
import com.model.Reservation;
import com.model.SocialSecurityNumber;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.fxml.FXMLLoader;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Used to control the reservation window's elements.
 * Uses ReservationView.fxml.
 */
public class ReservationController {
    private EntityManager em;

    @FXML
    private ComboBox<Analysis> analysisComboBox;

    @FXML
    private Label analysisDescriptionLabel;

    @FXML
    private Label analysisLabel;

    @FXML
    private TextField firstNameInput;

    @FXML
    private Label firstNameLabel;

    @FXML
    private TextField lastNameInput;

    @FXML
    private Label lastNameLabel;

    @FXML
    private TextField ssnInput;

    @FXML
    private Label ssnLabel;

    @FXML
    private Button submitButton;

    @FXML
    private Label titleLabel;

    @FXML
    private Label warningLabel;

    public FXMLLoader paymentLoader;

    public ReservationController() {
    }

    @FXML
    public void initialize(){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("lab_reservation");
        em = emf.createEntityManager();

        resetAnalysisComboBox();
        analysisComboBox.setOnAction(this::onAnalysisSelection);
    }

    /**
     * Empties and fills up combobox with all analysis.
     */
    private void resetAnalysisComboBox() {
        analysisComboBox.getItems().clear();
        analysisComboBox.getItems().add(null);
        analysisComboBox.getItems().addAll(AnalysisDao.getAllAnalysis(em));

        analysisDescriptionLabel.setVisible(false);
    }

    /**
     * Submits the information and passes to the next window.
     * @param unused unused
     */
    @FXML
    void onSubmit(ActionEvent unused) throws IOException {
        if (ssnInput.getText().length() != 15 || !ssnInput.getText().matches("[0-9]+") ||
            analysisComboBox.getSelectionModel().isEmpty() ||
            firstNameLabel.getText().length() == 0 ||
            lastNameLabel.getText().length() == 0
        ) {
            warningLabel.setVisible(true);
            warningLabel.setText("Error, please check your information. SSN must be 15 digits long and an analysis must be selected.");
        } else {
            Reservation reservation = null;
            try {
                reservation = ReservationDao.getBestReservationDate(em, analysisComboBox.getSelectionModel().getSelectedItem());
            } catch (NoSuchElementException e) {
                Alert errorDialog = new Alert(Alert.AlertType.ERROR);

                errorDialog.setTitle("Error!");
                errorDialog.setHeaderText("An error occurred while trying to find you a reservation !");
                errorDialog.setContentText(e.getMessage());

                errorDialog.showAndWait();
            }

            if (reservation != null) {
                Alert confirmationDialog = new Alert(Alert.AlertType.CONFIRMATION);

                confirmationDialog.setTitle("Reservation Proposition");
                confirmationDialog.setHeaderText("We have found the following reservation : " + reservation.getDate().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)));
                confirmationDialog.setContentText("Do you agree with the proposed reservation?");

                Optional<ButtonType> result = confirmationDialog.showAndWait();

                if (result.isPresent() && result.get() == ButtonType.OK) {
                    em.close();
                    LabReservation.getRoot().setCenter(paymentLoader.load());
                    PaymentController pc = paymentLoader.getController();
                    pc.setPatient(getPersonFromFields());
                    pc.setReservation(reservation);
                }
            }
        }
    }

    /**
     * Changes description text on analysis selection.
     * @param unused unused
     */
    @FXML
    void onAnalysisSelection(ActionEvent unused) {
        if (analysisComboBox.getSelectionModel().getSelectedItem() == null) {
            analysisDescriptionLabel.setVisible(false);
        } else {
            analysisDescriptionLabel.setVisible(true);
            analysisDescriptionLabel.setText(analysisComboBox.getSelectionModel().getSelectedItem().getDescription());
        }
    }

    private Person getPersonFromFields() {
        Person p = new Person();

        p.setFirstName(firstNameInput.getText());
        p.setLastName(lastNameInput.getText());
        p.setSsn(new SocialSecurityNumber(ssnInput.getText()));

        return p;
    }
}
