package com.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Represents the information related to a doctor.
 */
@Entity
public class Doctor extends Person {
    @Column
    private float salary;

    @ManyToMany(targetEntity = Analysis.class)
    @JoinTable(
            name = "DoctorAnalysis",
            joinColumns = @JoinColumn(name = "doctor_id"),
            inverseJoinColumns = @JoinColumn(name = "analysis_id")
    )
    private Set<Analysis> analysisList;

    public Doctor() {
        analysisList = new HashSet<>();
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    public Set<Analysis> getAnalysisList() {
        return analysisList;
    }

    public void setAnalysisList(Set<Analysis> analysisList) {
        this.analysisList = analysisList;
    }

    public void addAnalysis(Analysis analysis) {
        analysisList.add(analysis);
        analysis.getDoctors().add(this);
    }

    public void removeAnalysis(Analysis analysis) {
        analysisList.remove(analysis);
        analysis.getDoctors().remove(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Doctor doctor = (Doctor) o;

        return getId() == doctor.getId();
    }

    @Override
    public int hashCode() {
        return getId();
    }
}
