package com.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Represents the information related to a person.
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name="personPatient")
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected int id;

    @Column(length = 30)
    protected String firstName;

    @Column(length = 30)
    protected String lastName;

    @Embedded
    @Column
    protected SocialSecurityNumber ssn;

    @OneToMany(mappedBy = "person")
    private Set<Reservation> reservations;

    public Person() {
        reservations = new HashSet<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public SocialSecurityNumber getSsn() {
        return ssn;
    }

    public void setSsn(SocialSecurityNumber ssn) {
        this.ssn = ssn;
    }

    public Set<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(Set<Reservation> reservations) {
        this.reservations = reservations;
    }

    @Override
    public String toString() {
        return firstName + " " + lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        return getId() == person.getId();
    }

    @Override
    public int hashCode() {
        return getId();
    }
}
