package com.model;

import javax.persistence.Embeddable;
import java.util.Objects;

/**
 * Represents a social security number (SSN).
 * Makes sure the number has a length of 15.
 */
@Embeddable
public class SocialSecurityNumber {
    String number;

    public SocialSecurityNumber() {
    }

    public SocialSecurityNumber(String n) {
        setNumber(n);
    }

    /**
     * Sets the value of this SSN.
     * Will throw an exception if the number passed as parameter is not of length 15.
     * @param number New value of the SSN.
     */
    public void setNumber(String number) {
        if (!number.matches("[0-9]{15}")) {
            throw new IllegalArgumentException("A social security number must be of 15 characters.");
        } else {
            this.number = number;
        }
    }

    @Override
    public String toString() {
        return number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SocialSecurityNumber that = (SocialSecurityNumber) o;

        return Objects.equals(number, that.number);
    }

    @Override
    public int hashCode() {
        return number != null ? number.hashCode() : 0;
    }
}
