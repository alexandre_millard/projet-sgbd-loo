module com.application {
    requires javafx.base;
    requires javafx.controls;
    requires javafx.fxml;
    requires mysql.connector.java;
    requires java.sql;
    requires java.persistence;
    requires org.hibernate.orm.core;

    opens com.controller.lab_reservation to javafx.fxml;
    exports com.controller.lab_reservation to javafx.graphics;
    opens com.controller.lab_crud to javafx.fxml;
    opens com.model;

    exports com.model;
    exports com.application.lab_crud;
    exports com.application.lab_reservation;
}