CREATE TABLE IF NOT EXISTS analysis(
   id_analysis INT AUTO_INCREMENT,
   name_analysis VARCHAR(256) NOT NULL,
   description_analysis TEXT,
   PRIMARY KEY(id_analysis)
);

CREATE TABLE IF NOT EXISTS person(
   id_person INT AUTO_INCREMENT,
   firstname_person VARCHAR(256) NOT NULL,
   lastname_person VARCHAR(256) NOT NULL,
   secu_number_person CHAR(15) NOT NULL,
   PRIMARY KEY(id_person)
);

CREATE TABLE IF NOT EXISTS doctor(
   id_doctor INT AUTO_INCREMENT,
   salary_doctor REAL NOT NULL,
   id_person INT NOT NULL,
   PRIMARY KEY(id_doctor),
   FOREIGN KEY(id_person) REFERENCES person(id_person) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS reservation(
    id_reservation INT AUTO_INCREMENT,
    date_reservation DATE NOT NULL,
    id_doctor INT NOT NULL,
    id_analysis INT NOT NULL,
    id_person INT NOT NULL,
    PRIMARY KEY(id_reservation),
    FOREIGN KEY(id_doctor) REFERENCES doctor(id_doctor) ON DELETE CASCADE,
    FOREIGN KEY(id_analysis) REFERENCES analysis(id_analysis) ON DELETE CASCADE,
    FOREIGN KEY(id_person) REFERENCES person(id_person) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS can_perform(
   id_analysis INT,
   id_doctor INT,
   PRIMARY KEY(id_analysis, id_doctor),
   FOREIGN KEY(id_analysis) REFERENCES analysis(id_analysis) ON DELETE CASCADE,
   FOREIGN KEY(id_doctor) REFERENCES doctor(id_doctor) ON DELETE CASCADE
);
