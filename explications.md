# Découpage général du projet

Nous avons décidé de faire deux applications, un permettant de faire la réservation (point de vue du client) et l'autre permettant d'agir en CRUD sur les données (point de vue Administrateur).

Nous avons fait les interfaces graphiques avec JavaFX en séparant bien en structure MVC. 

Cela permet de profiter pleinement des avantages apportés par cette structure : modification facile, ajout et utilisation de plusieurs views, réutilisabilité.


# Décisions liées au Model
## Type d'analyse : 
Nous ne connaissons pas précisément les infos que veulent donner les médecins sur certaines analyses.
Nous avons donc décidé de représenter ces informations avec des champs nom/description. 
Dans un contexte plus professionnel, il faudrait demander au client afin d'être sûr de ce qu'il souhaite.

## Système d'authentification :
Nous avons supposé que le système d'authentification est géré en dehors de notre application. 
L'utilisateur arrive sur notre application, saisi ses données et nous les faisons vérifier au service externe d'authentification.
Donc l'utilisateur n'est seulement enregistré dans la base de données lorsqu'il a fini de réserver.
Par rapport aux coordonnées bancaires, on ne les enregistre pas et on considère que la vérification est faite en dehors de notre service (même si dans le code cela n'est bien sûr pas fait).